<?php
require("connect_db.php");
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <title>Torcedor de vantagens - clube</title>
    </head>
    <body>
        <div class="titulo-index">Clubes</div>
        <div class="menu">
            <a href="clubeCadastro.php">Cadastrar clube</a> | <a href="index.php">Voltar</a>
        </div>
        <table class="tabela-principal">
        <?php
            $arrayClubes = selectClube();
            if(empty($arrayClubes)){
                echo "<tr>
                        <td>Nenhum clube cadastrado</td>
                      </tr>";
            }else{
                echo "<tr>
                        <td><b>CLUBE:</b></td>
                        <td><b>AÇÕES</b></td>
                      </tr>";
                foreach ($arrayClubes as $key => $value) {
                    echo "<tr>
                            <td>".$value['nome_clube']."</td>
                            <td><a href=\"controller/clubeController.php?excluir=".$value['id']."\">Excluir</a></td>
                          </tr>";

                }
            }
        ?>
        </table>
    </body>
</html>

