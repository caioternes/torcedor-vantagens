<?php
function getPDO(){
    return new PDO("mysql:host=localhost;dbname=torcedor_vantagens", "root", "");
}

function selectClube(){
    $query_select = "SELECT * FROM clube_futebol";
    $array = getPDO()->query($query_select)->fetchAll();
    return $array;
}

function selectNomeClube($id){
    $query_select = "SELECT nome_clube FROM clube_futebol where id = ".$id;
    $array = getPDO()->query($query_select)->fetchAll();
    return $array;
}

function insertClube($nomeClube){
    $query_select = "INSERT INTO clube_futebol (nome_clube) VALUES (\"".$nomeClube."\")";
    return getPDO()->query($query_select);
}

function deleteClube($id){
    $query_select = "DELETE FROM clube_futebol WHERE id =".$id;
    return getPDO()->query($query_select);
}

function selectSocios(){
    $query_select = "SELECT * FROM socio";
    $array = getPDO()->query($query_select)->fetchAll();
    return $array;
}

function selectNomeSocio($id){
    $query_select = "SELECT nome_completo FROM socio where id = ".$id;
    $array = getPDO()->query($query_select)->fetchAll();
    return $array;
}

function insertSocio($nomeSocio){
    $query_select = "INSERT INTO socio (nome_completo) VALUES (\"".$nomeSocio."\")";
    return getPDO()->query($query_select);
}

function deleteSocio($id){
    $query_select = "DELETE FROM socio WHERE id =".$id;
    return getPDO()->query($query_select);
}

function selectSocioClube(){
    $query_select = "SELECT sc.id, s.nome_completo, c.nome_clube FROM socio_clube sc"
            . "     INNER JOIN socio s ON s.id = sc.socio"
            . "     INNER JOIN clube_futebol c ON c.id = sc.clube";
    $array = getPDO()->query($query_select)->fetchAll();
    return $array;
}

function insertSocioClube($socio, $clube){
    $query_select = "INSERT INTO socio_clube (socio, clube) VALUES (".$socio.", ".$clube.")";
    return getPDO()->query($query_select);
}

function deleteSocioClube($id){
    $query_select = "DELETE FROM socio_clube WHERE id =".$id;
    return getPDO()->query($query_select);
}
?>
