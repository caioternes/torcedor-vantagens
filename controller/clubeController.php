<?php
require("../connect_db.php");
if(isset($_GET['excluir'])){
    if(deleteClube($_GET['excluir'])){
        echo"<script language='javascript' type='text/javascript'>alert('Clube excluido com sucesso!');window.location.href='../clube.php'</script>";
    }else{
        echo"<script language='javascript' type='text/javascript'>alert('Algum erro ocorreu na exclusão do clube, tente novamente!');window.location.href='../clube.php';</script>";
    }
}else{
    if(!$_POST['clube']){
         echo"<script language='javascript' type='text/javascript'>alert('Clube deve ser preenchido');window.location.href='../clubeCadastro.php';</script>";
    }else{
        $clubes = selectClube();
        foreach ($clubes as $key => $value){
            if($value['nome_clube'] == $_POST['clube']){
                echo"<script language='javascript' type='text/javascript'>alert('Clube já cadastrado');window.location.href='../clube.php';</script>";

                return;
            }
        }
        if(insertClube($_POST['clube'])){
            echo"<script language='javascript' type='text/javascript'>alert('Clube cadastrado com sucesso!');window.location.href='../clube.php'</script>";
        }else{
            echo"<script language='javascript' type='text/javascript'>alert('Algum erro ocorreu no cadastro do clube, tente novamente!');window.location.href='../clube.php';</script>";
        }

    }
}
