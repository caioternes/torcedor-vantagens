<?php
require("../connect_db.php");
if(isset($_GET['excluir'])){
    if(deleteSocioClube($_GET['excluir'])){
        echo"<script language='javascript' type='text/javascript'>alert('Relacionamento excluido com sucesso!');window.location.href='../socioClube.php'</script>";
    }else{
        echo"<script language='javascript' type='text/javascript'>alert('Algum erro ocorreu na exclusão do relacionamento, tente novamente!');window.location.href='../socioClube.php';</script>";
    }
}else{
    $array = selectSocioClube();
    foreach ($array as $key => $value){
        if($value['nome_completo'] == selectNomeSocio($_POST['socio']) && $value['nome_clube'] == selectNomeClube($_POST['clube'])){
            echo"<script language='javascript' type='text/javascript'>alert('Relacionamento já cadastrado');window.location.href='../socioClube.php';</script>";

            return;
        }
    }
    if(insertSocioClube($_POST['socio'], $_POST['clube'])){
        echo"<script language='javascript' type='text/javascript'>alert('Relacionamento cadastrado com sucesso!');window.location.href='../socioClube.php'</script>";
    }else{
        echo"<script language='javascript' type='text/javascript'>alert('Algum erro ocorreu no cadastro do relacionamento, tente novamente!');window.location.href='../socioClube.php';</script>";
    }

}
