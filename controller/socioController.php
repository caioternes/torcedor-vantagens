<?php
require("../connect_db.php");
if(isset($_GET['excluir'])){
    if(deleteSocio($_GET['excluir'])){
        echo"<script language='javascript' type='text/javascript'>alert('Sócio excluido com sucesso!');window.location.href='../socio.php'</script>";
    }else{
        echo"<script language='javascript' type='text/javascript'>alert('Algum erro ocorreu na exclusão do sócio, tente novamente!');window.location.href='../socio.php';</script>";
    }
}else{
    if(!$_POST['socio']){
         echo"<script language='javascript' type='text/javascript'>alert('Sócio deve ser preenchido');window.location.href='../socioCadastro.php';</script>";
    }else{
        $socios = selectSocios();
        foreach ($socios as $key => $value){
            if($value['nome_completo'] == $_POST['socio']){
                echo"<script language='javascript' type='text/javascript'>alert('Sócio já cadastrado');window.location.href='../socio.php';</script>";

                return;
            }
        }
        if(insertSocio($_POST['socio'])){
            echo"<script language='javascript' type='text/javascript'>alert('Sócio cadastrado com sucesso!');window.location.href='../socio.php'</script>";
        }else{
            echo"<script language='javascript' type='text/javascript'>alert('Algum erro ocorreu no cadastro do sócio, tente novamente!');window.location.href='../socio.php';</script>";
        }

    }
}
