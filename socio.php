<?php
require("connect_db.php");
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <title>Torcedor de vantagens - sócio</title>
    </head>
    <body>
        <div class="titulo-index">Sócios</div>
        <div class="menu">
            <a href="socioCadastro.php">Cadastrar sócio</a> | <a href="index.php">Voltar</a>
        </div>
        <table class="tabela-principal">
        <?php
            $arraySocios = selectSocios();
            if(empty($arraySocios)){
                echo "<tr>
                        <td>Nenhum sócio cadastrado</td>
                      </tr>";
            }else{
                echo "<tr>
                        <td><b>Sócio:</b></td>
                        <td><b>AÇÕES:</b></td>
                      </tr>";
                foreach ($arraySocios as $key => $value) {
                    echo "<tr>
                            <td>".$value['nome_completo']."</td>
                            <td><a href=\"controller/socioController.php?excluir=".$value['id']."\">Excluir</a></td>
                          </tr>";
                }
            }
        ?>
        </table>
    </body>
</html>

