<?php
require("connect_db.php");
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <title>Torcedor de vantagens - Relacionamento sócio e clube</title>
    </head>
    <body>
        <DIV class="titulo-index">Relacionamento de sócios e clubes</DIV>
        <DIV class="menu">
            <a href="socioClubeCadastro.php">Cadastrar relacionamento de sócio e clube</a> | <a href="index.php">Voltar</a>
        </div>
        <table class="tabela-principal">
        <?php
            $arraySocioClube = selectSocioClube();
            if(empty($arraySocioClube)){
                echo "<tr>
                        <td>Nenhum relacionamento cadastrado</td>
                      </tr>";
            }else{
                echo "<tr>
                        <td><b>Sócio:</b></td>
                        <td><b>Clube:</b></td>
                        <td><b>AÇÕES:</b></td>
                      </tr>";
                foreach ($arraySocioClube as $key => $value) {
                    echo "<tr>
                            <td>".$value['nome_completo']."</td>
                            <td>".$value['nome_clube']."</td>
                            <td><a href=\"controller/socioClubeController.php?excluir=".$value['id']."\">Excluir</a></td>
                          </tr>";
                }
            }
        ?>
        </table>
    </body>
</html>

