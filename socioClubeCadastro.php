<?php
require("connect_db.php");
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <title>Torcedor de vantagens - cadastro relacionamento Sócio e Clube</title>
    </head>
    <body>
        <div class="titulo-index">Cadastro de Relacionamento Sócio e Clube</div>
        <div class="form">
            <form method="POST" action="controller/socioClubeController.php">
                <label>Sócio:</label>
                <select name="socio">
                <?php
                    $socios = selectSocios();
                    foreach ($socios as $key => $value){
                        echo '<option value="'.$value["id"].'">'.$value["nome_completo"].'</option>';
                    }        
                ?>    
                </select><br><br>
                <label>Clube:</label>
                <select name="clube">
                <?php
                    $clubes = selectClube();
                    foreach ($clubes as $key => $value){
                        echo '<option value="'.$value["id"].'">'.$value["nome_clube"].'</option>';
                    }        
                ?>    
                </select><br><br>
                <input type="submit" value="Cadastrar" id="cadastrar" name="cadastrar">
            </form>
        </div>
    </body>
</html>

